#///////////////////////////////////////////////////////////////////////////////
#  Copyright (c) 2017 Clemson University.
# 
#  This file was originally written by Bradley S. Meyer.
# 
#  This is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
# 
#  This software is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this software; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#  USA
# 
#/////////////////////////////////////////////////////////////////////////////*/

#///////////////////////////////////////////////////////////////////////////////
#//!
#//! \file Makefile
#//! \brief A makefile to generate starlib utilities.
#//!
#///////////////////////////////////////////////////////////////////////////////

#///////////////////////////////////////////////////////////////////////////////
# Here are lines to be edited, if desired.
#///////////////////////////////////////////////////////////////////////////////

SVNURL = svn://svn.code.sf.net/p/nucnet-tools/code/trunk

NUCNET_TARGET = ../nucnet-tools-code
VENDORDIR = $(NUCNET_TARGET)/vendor
OBJDIR = $(NUCNET_TARGET)/obj

NNT_DIR = $(NUCNET_TARGET)/nnt
BUILD_DIR = $(NUCNET_TARGET)/build

#///////////////////////////////////////////////////////////////////////////////
# End of lines to be edited.
#///////////////////////////////////////////////////////////////////////////////

#===============================================================================
# Svn.
#===============================================================================

SVN_CHECKOUT := $(shell if [ ! -d $(NUCNET_TARGET) ]; then svn co $(SVNURL) $(NUCNET_TARGET); else svn update $(NUCNET_TARGET); fi )

#///////////////////////////////////////////////////////////////////////////////
# End of lines to be edited.
#///////////////////////////////////////////////////////////////////////////////

#===============================================================================
# My user.
#===============================================================================

include $(BUILD_DIR)/Makefile

ifdef BOOST_MT
  CLIBS += -lboost_system-mt -lboost_filesystem-mt
else
  CLIBS += -lboost_system -lboost_filesystem
endif

VPATH = $(BUILD_DIR):$(NNT_DIR)

STARLIB_UTILITIES_OBJS = $(WN_OBJ)           	\
                $(NNT_OBJ)

#===============================================================================
# Executables.
#===============================================================================

STARLIB_UTILITIES_EXEC = splitter joiner \

$(STARLIB_UTILITIES_EXEC): $(STARLIB_UTILITIES_OBJS)
	$(CC) $(STARLIB_UTILITIES_OBJS) -o $(BINDIR)/$@ $@.cpp $(CLIBS)

.PHONY all_starlib_utilities: $(STARLIB_UTILITIES_EXEC)

#===============================================================================
# Clean up. 
#===============================================================================

.PHONY: clean_starlib_utilities

clean_starlib_utilities:
	rm -f $(OBJ)
