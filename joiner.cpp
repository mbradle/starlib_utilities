///////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Code to join individual files in chapters into a single starlib file.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#include <fstream>
#include <iostream>

#include <boost/filesystem.hpp>

//##############################################################################
// main().
//##############################################################################

int
main( int argc, char * argv[] )
{

  std::ifstream input_file;
  std::ofstream output_file;
  std::string line;

  //============================================================================
  // Check input.
  //============================================================================

  if( argc != 3 )
  {

    std::cerr << std::endl;
    std::cerr << "Usage: " << argv[0] << " input_dir output_file " <<
      std::endl << std::endl;
    std::cerr << "  input_dir = input starlib reaction directory" <<
      std::endl << std::endl;
    std::cerr << "  output_file = output text file" <<
      std::endl << std::endl;

    return EXIT_FAILURE;

  }

  //============================================================================
  // Create directory.
  //============================================================================

  boost::filesystem::path p( argv[1] );

  if( !boost::filesystem::exists( p ) )
  {
    std::cerr << "Output directory does not exist.\n";
    exit( EXIT_FAILURE );
  }

  //============================================================================
  // Iterate directory.
  //============================================================================
  
  output_file.open( argv[2] );

  if( boost::filesystem::is_directory( p ) )
  {
    for(
      boost::filesystem::recursive_directory_iterator dir_itr( p ), end_iter;
      dir_itr != end_iter;
      ++dir_itr
    )
    {
      try
      {
        if( boost::filesystem::is_regular_file( dir_itr->status() ) )
        {
          input_file.open( dir_itr->path().string().c_str() );
          while( std::getline( input_file, line ) )
          {
            output_file << line << "\n";
          }
          input_file.close();
        }
      }
      catch (const std::exception & ex)
      {
        std::cout << ex.what() << std::endl;
      }
    }
  }

  output_file.close();

  return EXIT_SUCCESS;

}
