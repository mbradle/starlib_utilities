///////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Code to convert single starlib file to individual files in chapter
//!        directories.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#include <fstream>
#include <iostream>

#include <boost/filesystem.hpp>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/tuple/tuple.hpp>

//##############################################################################
// Defines.
//##############################################################################

#define I_LEN  60

std::pair<std::string, std::string>
get_chapter_and_file_name(
  const std::string& line
)
{

  std::string s_chapter, s_source;
  int i_chapter;
  std::vector<std::string> s(6);
  size_t i_reac, i_total;
  std::vector<std::string> reactants, products;

  s_chapter = line.substr(0,3);
  boost::trim( s_chapter );
  i_chapter = boost::lexical_cast<int>( s_chapter );
  s_chapter = "chapter_" + s_chapter;

  s_source = line.substr(43,4);
  boost::trim( s_source );

  for( size_t i = 0; i < 6; i++ )
  {
    s[i] = line.substr( 5+i*5, 5 );
    boost::trim( s[i] );
  }

  switch( i_chapter )
  {

    case 1:
      i_reac = 1;
      i_total = 2;
      break;
    case 2:
      i_reac = 1;
      i_total = 3;
      break;
    case 3:
      i_reac = 1;
      i_total = 4;
      break;
    case 4:
      i_reac = 2;
      i_total = 3;
      break;
    case 5:
      i_reac = 2;
      i_total = 4;
      break;
    case 6:
      i_reac = 2;
      i_total = 5;
      break;
    case 7:
      i_reac = 2;
      i_total = 6;
      break;
    case 8:
      i_reac = 3;
      i_total = 4;
      break;
    case 9:
      i_reac = 3;
      i_total = 5;
      break;
    case 10:
      i_reac = 4;
      i_total = 6;
      break;
    case 11:
      i_reac = 1;
      i_total = 5;
      break;
    default:
      std::cerr << "No such chapter." << std::endl;
      exit( EXIT_FAILURE );
  } 

  for( size_t i = 0; i < i_total; i++ )
  {
    if( i < i_reac )
      reactants.push_back( s[i] );
    else
      products.push_back( s[i] );
  }

  std::string s_file = "";

  for( size_t i = 0; i < reactants.size(); i++ )
  {
    s_file += reactants[i];
    if( i < reactants.size() - 1 )
    {
      s_file += "_";
    }
    else
    {
      s_file += "_to_";
    }
  }

  for( size_t i = 0; i < products.size(); i++ )
  {
    s_file += products[i] + "_";
  }

  s_file += s_source + ".txt";

  return std::make_pair( s_chapter, s_file );

}

//##############################################################################
// main().
//##############################################################################

int
main( int argc, char * argv[] )
{

  std::ifstream input_file;
  std::ofstream output_file;
  std::string line;

  //============================================================================
  // Check input.
  //============================================================================

  if( argc != 3 )
  {

    std::cerr << std::endl;
    std::cerr << "Usage: " << argv[0] << " input_text_file output_dir " <<
      std::endl << std::endl;
    std::cerr << "  input_text_file = input starlib reaction data file" <<
      std::endl << std::endl;
    std::cerr << "  output_dir = output directory" <<
      std::endl << std::endl;

    return EXIT_FAILURE;

  }

  //============================================================================
  // Create directory.
  //============================================================================

  boost::filesystem::path p1( argv[2] );

  if( boost::filesystem::exists( p1 ) )
  {
    std::cerr << "Parent output directory already exists.\n";
    exit( EXIT_FAILURE );
  }
  else
  {
    boost::filesystem::create_directory( p1 );
  }

  //============================================================================
  // Open file.
  //============================================================================

  input_file.open( argv[1] );

  if( !input_file.good() )
  {
    std::cerr << "Couldn't open input file " << argv[1] << "." << std::endl;
    return EXIT_FAILURE;
  }

  //============================================================================
  // Loop on the data.
  //============================================================================

  while( std::getline( input_file, line ) )
  {

    std::string s_chapter, s_file;

    boost::tie( s_chapter, s_file ) = get_chapter_and_file_name( line );

    std::string s_file_path = std::string( argv[2] ) + "/" + s_chapter;

    boost::filesystem::path p2( s_file_path );

    if( !boost::filesystem::exists( p2 ) )
    {
      boost::filesystem::create_directory( p2 );
    }

    s_file_path += "/" + s_file;

    output_file.open( s_file_path.c_str() );

    output_file << line << "\n";

    for( size_t i = 0; i < I_LEN; i++ )
    {
      std::getline( input_file, line ); 
      output_file << line << "\n";
    }

    output_file.close();

  }

  input_file.close();

  return EXIT_SUCCESS;

}

